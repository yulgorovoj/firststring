#include <iostream>
#include <string>

int main()
{
    std::string str = "Hello, world";
    std::cout << str << '\n' 
        << str.length() << '\n'
        << str[0] << '\n' 
        << str[str.length() - 1] << std::endl;
}